

namespace Empresa
{
    public class Principal
    {
        public static void Main(String args[])
        {


        }
    }

    public class IEntity
    {
        public Guid {get;set;}
    }

    public class IStore
    {
        public static Create(IEntity ent);

        public IEnumerable<IEntity> GetAll();

        public static Update(IEntity ent);

        public static Remove(IEntity ent);

    } 
}