﻿using MainConsole.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainConsole.BusinessModels
{
    public class Transaction : IUniqueId
    {

        public Guid Id { get; set; }
        public Guid TransactionTypeId { get; set; }
        public Guid OrigAccountId { get; set; }
        public float OrigPrevBalance { get; set; }
        public float OrigNewBalance { get; set; }
        public float Ammount { get; set; }
        public Guid DestAccountId { get; set; }
        public float DestPrevBalance { get; set; }
        public float  DestNewBalance { get; set; }
        public bool IsFraud { get; set; }
        public bool IsFlaggedFraud { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public Guid GetId()
        {
            return this.Id;
        }

        public void SetId(Guid guid)
        {
            this.Id = guid;
        }
    }


}
