﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MainConsole.Interfaces;

namespace MainConsole.BusinessModels
{
    public class AccountType : IUniqueId
    {

        public Guid Id { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }


        public Guid GetId()
        {
            return Id;
        }

        public void SetId(Guid g)
        {
            this.Id = g;
        }
    }


}
