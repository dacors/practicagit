﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MainConsole.Interfaces;

namespace MainConsole.BusinessModels
{
    class Account : IUniqueId
    {
        public Guid Id { get; set; }

        public int AccountTypeId { get; set; }

        public string Name { get; set; }

        public float CurrentBalance { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }


        public Guid GetId()
        {
            return Id;
        }

        public void SetId(Guid g)
        {
            this.Id = g;    
        }
    }
}
