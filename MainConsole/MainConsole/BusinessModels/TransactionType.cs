﻿using MainConsole.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainConsole.BusinessModels
{
    public class TransactionType :IUniqueId
    {
        /*
        [Id] [uniqueidentifier] NOT NULL,
	    [Description] [nvarchar](20) NOT NULL,
	    [CreatedDate] [datetime] NOT NULL,
	    [UpdatedDate] [datetime] NULL,
        */
        Guid Id { get; set; }

        string Description { get; set; }

        DateTime CreatedDate { get; set; }

        DateTime UpdatedDate { get; set; }

        public Guid GetId()
        {
            return this.Id;
        }

        public void SetId(Guid guid)
        {
            this.Id = guid;
        }
    }
}
