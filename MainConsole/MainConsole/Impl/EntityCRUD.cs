﻿using MainConsole.BusinessModels;
using MainConsole.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainConsole.Impl
{
    public class EntityCRUD : IStore
    {
        private static List<IUniqueId> lsEntity = new List<IUniqueId>();

        /// <summary>
        /// William Cortes
        /// 14-16-2018
        /// Metodo para la adicion de datos
        /// </summary>
        /// <param name="ent"></param>
        /// <returns>bool</returns>
        public bool Save(IUniqueId ent)
        {
            bool success;
            try
            {
                lsEntity.Add(ent);
                success = true;
            }
            catch (Exception ex)
            {
                success = false;
                throw ex;
            }

            return success;
        }

        /// <summary>
        /// William Cortes
        /// 14-06-2018
        /// Metodo que retorna todos los datos
        /// </summary>
        /// <returns>Lista de IUniqueId</returns>
        public IEnumerable<IUniqueId> GetAll()
        {
            return lsEntity;
        }

        /// <summary>
        /// William Cortes
        /// 14-06-2018
        /// Metodo para el conteo de los datos
        /// </summary>
        /// <returns>Numero de datos</returns>
        public int CountAll()
        {
            return lsEntity.Count();
        }

        public IUniqueId Find(IUniqueId entToFindById)
        {
            IUniqueId resp;

            if (entToFindById == null)
            {
                throw new ArgumentNullException("Parametro ent no puede ser nulo");
            }

            resp = null;

            if ((lsEntity != null) && lsEntity.Count > 0)
            {
                resp = lsEntity.FirstOrDefault(x => x.GetId().Equals(entToFindById.GetId()));
            }

            return resp;
        }

        /// <summary>
        /// William Cortes
        /// 14-16-2018
        /// Metodo para la eliminacion de datos
        /// </summary>
        /// <param name="ent"></param>
        /// <returns>bool</returns>
        public bool Remove(IUniqueId ent)
        {
            bool success;

            try
            {
                lsEntity.Remove(ent);
                success = true;
            }
            catch (Exception ex)
            {
                success = false;
                throw ex;
            }

            return success;
        }

        /// <summary>
        /// William Cortes
        /// 14-16-2018
        /// Metodo para la actualizacion de datos
        /// </summary>
        /// <param name="entityToUpdate"></param>
        /// <returns>Bool</returns>
        public bool Update(IUniqueId entityToUpdate)
        {
            bool sucess;
            IUniqueId entityOld;
            try
            {
                if (entityToUpdate != null)
                {
                    entityOld = lsEntity.FirstOrDefault(x => x.GetId().Equals(entityToUpdate.GetId()));
                    lsEntity.Remove(entityOld);
                    lsEntity.Add(entityToUpdate);
                }
                sucess = true;
            }
            catch (Exception ex)
            {
                sucess = false;
                throw ex;
            }

            return sucess;

        }


    }
}
