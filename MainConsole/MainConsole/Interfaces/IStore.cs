﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainConsole.Interfaces
{
    public interface IStore
    {
        bool Save(IUniqueId ent);

        IEnumerable<IUniqueId> GetAll();

        IUniqueId Find(IUniqueId entToFindById);

        bool Update(IUniqueId ent);

        bool Remove(IUniqueId ent);

    }


}
