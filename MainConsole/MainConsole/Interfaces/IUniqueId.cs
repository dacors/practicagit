﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainConsole.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUniqueId
    {
        Guid GetId();

        void SetId(Guid g);
    }
}
