USE [TransFraud]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 08/06/2018 06:06:06 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[Id] [uniqueidentifier] NOT NULL,
	[AccountTypeId] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CurrentBalance] [decimal](12, 3) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountType]    Script Date: 08/06/2018 06:06:06 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountType](
	[Id] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](20) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_AccountType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 08/06/2018 06:06:06 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[Id] [uniqueidentifier] NOT NULL,
	[Step] [int] NOT NULL,
	[TransactionTypeId] [uniqueidentifier] NOT NULL,
	[OrigAccountId] [uniqueidentifier] NOT NULL,
	[OrigPrevBalance] [decimal](12, 3) NOT NULL,
	[OrigNewBalance] [decimal](12, 3) NOT NULL,
	[Amount] [decimal](12, 3) NOT NULL,
	[DestAccountId] [uniqueidentifier] NOT NULL,
	[DestPrevBalance] [decimal](12, 3) NOT NULL,
	[DestNewBalance] [decimal](12, 3) NOT NULL,
	[IsFraud] [bit] NOT NULL,
	[IsFlaggedFraud] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TransactionType]    Script Date: 08/06/2018 06:06:06 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionType](
	[Id] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](20) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_TransactionType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Account] ([Id], [AccountTypeId], [Name], [CurrentBalance], [CreatedDate], [UpdatedDate]) VALUES (N'0102371a-7da3-4593-83b8-0794f44e6200', N'd18bacbd-4ad1-457b-bcd0-4fe0e9e443b5', N'C1305486145', CAST(0.000 AS Decimal(12, 3)), CAST(N'2018-06-08T06:03:12.427' AS DateTime), NULL)
INSERT [dbo].[Account] ([Id], [AccountTypeId], [Name], [CurrentBalance], [CreatedDate], [UpdatedDate]) VALUES (N'22845371-2eb0-43e6-970e-0b03dbefc547', N'd18bacbd-4ad1-457b-bcd0-4fe0e9e443b5', N'C1666544295', CAST(0.000 AS Decimal(12, 3)), CAST(N'2018-06-08T06:03:12.427' AS DateTime), NULL)
INSERT [dbo].[Account] ([Id], [AccountTypeId], [Name], [CurrentBalance], [CreatedDate], [UpdatedDate]) VALUES (N'e366a249-0bd1-451b-8fdd-2b20c255c6e8', N'c19bb0d4-7f97-4d4b-9c71-1d620281edbe', N'M1979787155', CAST(0.000 AS Decimal(12, 3)), CAST(N'2018-06-08T06:03:12.397' AS DateTime), NULL)
INSERT [dbo].[Account] ([Id], [AccountTypeId], [Name], [CurrentBalance], [CreatedDate], [UpdatedDate]) VALUES (N'0fb6f8f9-cbd9-45d6-993a-2bed3c9f14ce', N'd18bacbd-4ad1-457b-bcd0-4fe0e9e443b5', N'C1231006815', CAST(0.000 AS Decimal(12, 3)), CAST(N'2018-06-08T06:03:12.427' AS DateTime), NULL)
INSERT [dbo].[Account] ([Id], [AccountTypeId], [Name], [CurrentBalance], [CreatedDate], [UpdatedDate]) VALUES (N'4aa04da5-3b06-45c2-85ef-bb8d848ef969', N'd18bacbd-4ad1-457b-bcd0-4fe0e9e443b5', N'C553264065', CAST(0.000 AS Decimal(12, 3)), CAST(N'2018-06-08T06:03:12.443' AS DateTime), NULL)
INSERT [dbo].[Account] ([Id], [AccountTypeId], [Name], [CurrentBalance], [CreatedDate], [UpdatedDate]) VALUES (N'fbd51536-8e57-4164-886c-eaee76aa4f2a', N'c19bb0d4-7f97-4d4b-9c71-1d620281edbe', N'M2044282225', CAST(0.000 AS Decimal(12, 3)), CAST(N'2018-06-08T06:03:12.410' AS DateTime), NULL)
INSERT [dbo].[AccountType] ([Id], [Description], [CreatedDate], [UpdatedDate]) VALUES (N'c19bb0d4-7f97-4d4b-9c71-1d620281edbe', N'CHECKING', CAST(N'2018-06-08T05:51:47.797' AS DateTime), NULL)
INSERT [dbo].[AccountType] ([Id], [Description], [CreatedDate], [UpdatedDate]) VALUES (N'd18bacbd-4ad1-457b-bcd0-4fe0e9e443b5', N'SAVING', CAST(N'2018-06-08T05:51:47.810' AS DateTime), NULL)
INSERT [dbo].[TransactionType] ([Id], [Description], [CreatedDate], [UpdatedDate]) VALUES (N'8e8592e6-20e9-4dea-91da-130a62761e70', N'TRANSFER', CAST(N'2018-06-08T05:49:43.997' AS DateTime), NULL)
INSERT [dbo].[TransactionType] ([Id], [Description], [CreatedDate], [UpdatedDate]) VALUES (N'a34fecb8-b31b-478e-b02c-7150b155f6b7', N'DEBIT', CAST(N'2018-06-08T05:49:43.980' AS DateTime), NULL)
INSERT [dbo].[TransactionType] ([Id], [Description], [CreatedDate], [UpdatedDate]) VALUES (N'0f2a2993-214b-4be4-b737-79a051ad772b', N'CASH_OUT', CAST(N'2018-06-08T05:49:43.997' AS DateTime), NULL)
INSERT [dbo].[TransactionType] ([Id], [Description], [CreatedDate], [UpdatedDate]) VALUES (N'abde5fd0-f74e-429c-b419-bb7f452fb716', N'PAYMENT', CAST(N'2018-06-08T05:49:43.980' AS DateTime), NULL)
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_AccountType] FOREIGN KEY([AccountTypeId])
REFERENCES [dbo].[AccountType] ([Id])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_AccountType]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Account_Dest] FOREIGN KEY([DestAccountId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Account_Dest]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Account_Orig] FOREIGN KEY([OrigAccountId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Account_Orig]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_TransactionType] FOREIGN KEY([TransactionTypeId])
REFERENCES [dbo].[TransactionType] ([Id])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_TransactionType]
GO
