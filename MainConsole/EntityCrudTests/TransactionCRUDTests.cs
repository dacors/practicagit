﻿using System;
using System.Linq;
using MainConsole.BusinessModels;
using MainConsole.Impl;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EntityCrudTests
{
    [TestClass]
    public class TransactionCRUDTests
    {
        private static EntityCRUD transCRUD = new EntityCRUD();

        [TestMethod]
        public void TransactionSaveTest()
        {
            Transaction t;

            int cont;

            cont = transCRUD.CountAll();

            t = new Transaction()
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTime.Now,
            };

            transCRUD.Save(t);

            Assert.AreEqual(transCRUD.CountAll(), cont + 1);

            TransactionRemoveTest(t);

        }

        private void TransactionUpdateTest(Transaction tToRemove)
        {
            /*
            Transaction t;
            int cont;

            cont = transCRUD.CountAll();

            transCRUD.Remove(tToRemove);

            Assert.AreEqual(transCRUD.CountAll(), cont - 1);
            */

        }

        private void TransactionRemoveTest(Transaction tToRemove)
        {
            Transaction t;
            int cont;

            cont = transCRUD.CountAll();

            transCRUD.Remove(tToRemove);

            Assert.AreEqual(transCRUD.CountAll(), cont - 1);

        }
    }
}
